= {page-component-title}
include::partial$alias.adoc[]

== Page Coordinates

page-component-name: {page-component-name}

page-component-version: {page-component-version}

page-module: {page-module}

page-relative: {page-relative}

page-aliases: {page-aliases}

== Page Aliases

The page-aliases project demonstrates all possible forms of page references in the page-aliases attribute

== A Topic Page

This page includes the page-aliases attribute from a partial: the content included is:

[source,asciidoc]
----
:page-aliases: v2@{page-component-name}:old-module:{page-relative}
----

This module otherwise does not exist.

link:../../../v2/old-module/topic/topic.html[direct link to component2/v2/old-module/topic/topic.html]

